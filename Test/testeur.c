/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   testeur.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ehautefa <ehautefa@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/07 08:28:38 by ehautefa          #+#    #+#             */
/*   Updated: 2021/01/22 10:21:25 by ehautefa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

// %c :	conversion en unsigned char puis affichage caractère correspondant (caractère d'espacement y compris)
// %s : %s	char* chaîne de caractères (sans le '\0')
// %p :	Pour imprimer un pointeur (selon l'implantation) void*
// %d : conversion en int puis affichage entier en notation décimale signée
// %i : conversion en int puis affichage entier en notation décimale signée
// %u : conversion en int puis affichage  entier en notation décimale non signée
// %x : conversion en int puis affichage entier sous forme héxadécimale non signée 
// (non précédé de 0x ou 0X), en utilisant abcdef pour x et ABCDEF pour X
// %X :

//// 0 : d, i, u, X, x
// indique le remplissage avec des zéros. 
// Pour les conversions d, i, u, x, X, la valeur est complétée à gauche 
// avec des zéros plutôt qu'avec des espaces. Si les attributs 0 et - 
// apparaissent ensemble, l'attribut 0 est ignoré. Si une précision est fournie 
// avec une conversion numérique (d, i, u, x, et X), l'attribut 0 est ignoré. 
// Pour les autres conversions, le comportement est indéfini.
//// - :
// indique que la valeur doit être justifiée sur la limite gauche du champ 
// (par défaut elle l'est à droite). Sauf pour la conversion n, les valeurs 
// sont complétées à droite par des espaces, plutôt qu'à gauche par des zéros
//  ou des blancs. Un attribut - surcharge un attribut 0 si les deux sont fournis.

// http://manpagesfr.free.fr/man/man3/rex = printf.3.html

int main(void)
{
	char a = 'a';
	int ret;
	char b = '\0';
	int c = 'a';
	int d = 177;
	int da = -177;
	char	*e = NULL;
	char *f = "";
	char *g = "ABCDEFGHIJKLMNOPQRSTUWXYZ";
	int rex;

	ret = ft_printf("---------------- TEST 7 --------------\n\n");
	ret = ft_printf("%p\n", g);
	rex = printf("%p\n", g);
	rex = printf("%d != %d\n",ret, rex);


	ret = ft_printf("---------------- TEST 8 --------------\n\n");
	ret = ft_printf("%c %s %p %d %i %u %x %X\n", a,g,g,d,a,a,a,a);
	rex = printf("%c %s %p %d %i %u %x %X\n", a,g,g,d,a,a,a,a);
	rex = printf("%d != %d\n",ret, rex);

	ret = ft_printf("---------------- TEST 9 --------------\n\n");
	ret = ft_printf("%-6c %-s %-p %-d %-i %-u %-x %-X\n", a,g,g,d,a,a,a,a);
	rex = printf("%-6c %-s %-p %-d %-i %-u %-x %-X\n", a,g,g,d,a,a,a,a);
	rex = printf("%d != %d\n",ret, rex);

	ret = ft_printf("---------------- TEST 10 --------------\n\n");
	ret = ft_printf("%0c %s %p %06d %0i %0u %0x %0X\n", a,g,g,d,a,a,a,a);
	rex = printf("%0c %s %p %06d %0i %0u %0x %0X\n", a,g,g,d,a,a,a,a);
	rex = printf("%d != %d\n",ret, rex);

	ret = ft_printf("---------------- TEST 11 --------------\n\n");
	ret = ft_printf("%-8.%$\n");
	rex = printf("%-8.%$\n");
	rex = printf("%d != %d\n",ret, rex);

	ret = ft_printf("%-40.*%$\n",42);
	rex = printf("%-40.*%$\n",42);
	rex = printf("%d != %d\n",ret, rex);

	ret = ft_printf("%-*%$\n",42);
	rex = printf("%-*%$\n",42);
	rex = printf("%d != %d\n",ret, rex);

	ret = ft_printf("%*%$\n",42);
	rex = printf("%*%$\n",42);
	rex = printf("%d != %d\n",ret, rex);

	ret = ft_printf("%10.5%$\n");
	rex = printf("%10.5%$\n");
	rex = printf("%d != %d\n",ret, rex);

	ret = ft_printf("%05%$\n");
	rex = printf("%05%$\n");
	rex = printf("%d != %d\n",ret, rex);

	ret = ft_printf("%-05%$\n");
	rex = printf("%-05%$\n");
	rex = printf("%d != %d\n",ret, rex);
	
	ret = ft_printf("---------------- TEST 13 --------------\n\n");
	ret = ft_printf("%-8.x$\n", c);
	rex = printf("%-8.x$\n",c);
	rex = printf("%d != %d\n",ret, rex);
	ret = ft_printf("%-8.x$\n", d);
	rex = printf("%-8.x$\n",d);
	rex = printf("%d != %d\n",ret, rex);
	ret = ft_printf("%-8.x$\n", da);
	rex = printf("%-8.x$\n",da);
	rex = printf("%d != %d\n",ret, rex);

	ret = ft_printf("%12x$\n", c);
	rex = printf("%12x$\n",c);
	rex = printf("%d != %d\n",ret, rex);
	ret = ft_printf("%12x$\n", d);
	rex = printf("%12x$\n",d);
	rex = printf("%d != %d\n",ret, rex);
	ret = ft_printf("%12x$\n", da);
	rex = printf("%12x$\n",da);
	rex = printf("%d != %d\n",ret, rex);

	ret = ft_printf("%012.4x$\n", c);
	rex = printf("%12.4x$\n",c);
	rex = printf("%d != %d\n",ret, rex);
	ret = ft_printf("%012.4x$\n", d);
	rex = printf("%12.4x$\n",d);
	rex = printf("%d != %d\n",ret, rex);
	ret = ft_printf("%012.4x$\n", da);
	rex = printf("%12.4x$\n",da);
	rex = printf("%d != %d\n",ret, rex);


	ret = ft_printf("%0.4x$\n", c);
	rex = printf("%0.4x$\n",c);
	rex = printf("%d != %d\n",ret, rex);
	ret = ft_printf("%0.4x$\n", d);
	rex = printf("%0.4x$\n",d);
	rex = printf("%d != %d\n",ret, rex);
	ret = ft_printf("%0.4x$\n", da);
	rex = printf("%0.4x$\n",da);
	rex = printf("%d != %d\n",ret, rex);

	ret = ft_printf("%-40.*x$\n",42, c);
	rex = printf("%-40.*x$\n",42,c);
	rex = printf("%d != %d\n",ret, rex);
	ret = ft_printf("%-40.*x$\n",42, d);
	rex = printf("%-40.*x$\n",42,d);
	rex = printf("%d != %d\n",ret, rex);
	ret = ft_printf("%-40.*x$\n",42, da);
	rex = printf("%-40.*x$\n",42, da);
	rex = printf("%d != %d\n",ret, rex);

	ret = ft_printf("%-*x$\n",42, c);
	rex = printf("%-*x$\n",42,c);
	rex = printf("%d != %d\n",ret, rex);
	ret = ft_printf("%-*x$\n",42, d);
	rex = printf("%-*x$\n",42,d);
	rex = printf("%d != %d\n",ret, rex);
	ret = ft_printf("%-*x$\n",42, da);
	rex = printf("%-*x$\n",42, da);
	rex = printf("%d != %d\n",ret, rex);

	ret = ft_printf("%0*x$\n",42, c);
	rex = printf("%0*x$\n",42,c);
	rex = printf("%d != %d\n",ret, rex);
	ret = ft_printf("%0*x$\n",42, d);
	rex = printf("%0*x$\n",42,d);
	rex = printf("%d != %d\n",ret, rex);
	ret = ft_printf("%0*x$\n",42, da);
	rex = printf("%0*x$\n",42, da);
	rex = printf("%d != %d\n",ret, rex);
	
	ret = ft_printf("%10.5x$\n", 2);
	rex = printf("%10.5x$\n", 2);
	rex = printf("%d != %d\n",ret, rex);

	ret = ft_printf("---------------- TEST 15 --------------\n\n");
	ret = ft_printf("%-8.d$\n", c);
	rex = printf("%-8.d$\n",c);
	rex = printf("%d != %d\n",ret, rex);
	ret = ft_printf("%-8.d$\n", d);
	rex = printf("%-8.d$\n",d);
	rex = printf("%d != %d\n",ret, rex);
	ret = ft_printf("%-8.d$\n", da);
	rex = printf("%-8.d$\n",da);
	rex = printf("%d != %d\n",ret, rex);



	ret = ft_printf("%-8.0d$\n", c);
	rex = printf("%-8.0d$\n",c);
	rex = printf("%d != %d\n",ret, rex);
	ret = ft_printf("%-8.0d$\n", d);
	rex = printf("%-8.0d$\n",d);
	rex = printf("%d != %d\n",ret, rex);
	ret = ft_printf("%-8.0d$\n", da);
	rex = printf("%-8.0d$\n",da);
	rex = printf("%d != %d\n",ret, rex);

	ret = ft_printf("%12d$\n", c);
	rex = printf("%12d$\n",c);
	rex = printf("%d != %d\n",ret, rex);
	ret = ft_printf("%12d$\n", d);
	rex = printf("%12d$\n",d);
	rex = printf("%d != %d\n",ret, rex);
	ret = ft_printf("%12d$\n", da);
	rex = printf("%12d$\n",da);
	rex = printf("%d != %d\n",ret, rex);

	ret = ft_printf("%012.4d$\n", c);
	rex = printf("%012.4d$\n",c);
	rex = printf("%d != %d\n",ret, rex);
	ret = ft_printf("%012.4d$\n", d);
	rex = printf("%012.4d$\n",d);
	rex = printf("%d != %d\n",ret, rex);
	ret = ft_printf("%012.4d$\n", da);
	rex = printf("%012.4d$\n",da);
	rex = printf("%d != %d\n",ret, rex);


	ret = ft_printf("%0.4d$\n", c);
	rex = printf("%0.4d$\n",c);
	rex = printf("%d != %d\n",ret, rex);
	ret = ft_printf("%0.4d$\n", d);
	rex = printf("%0.4d$\n",d);
	rex = printf("%d != %d\n",ret, rex);
	ret = ft_printf("%0.4d$\n", da);
	rex = printf("%0.4d$\n",da);
	rex = printf("%d != %d\n",ret, rex);

	ret = ft_printf("%-40.*d$\n",42, c);
	rex = printf("%-40.*d$\n",42,c);
	rex = printf("%d != %d\n",ret, rex);
	ret = ft_printf("%-40.*d$\n",42, d);
	rex = printf("%-40.*d$\n",42,d);
	rex = printf("%d != %d\n",ret, rex);
	ret = ft_printf("%-40.*d$\n",42, da);
	rex = printf("%-40.*d$\n",42, da);
	rex = printf("%d != %d\n",ret, rex);

	ret = ft_printf("%-2.7d$\n",c);
	rex = printf("%-2.7d$\n",c);
	rex = printf("%d != %d\n",ret, rex);
	ret = ft_printf("%-2.7d$\n", d);
	rex = printf("%-2.7d$\n",d);
	rex = printf("%d != %d\n",ret, rex);
	ret = ft_printf("%-2.7d$\n", da);
	rex = printf("%-2.7d$\n", da);
	rex = printf("%d != %d\n",ret, rex);

	ret = ft_printf("%-*d$\n",42, c);
	rex = printf("%-*d$\n",42,c);
	rex = printf("%d != %d\n",ret, rex);
	ret = ft_printf("%-*d$\n",42, d);
	rex = printf("%-*d$\n",42,d);
	rex = printf("%d != %d\n",ret, rex);
	ret = ft_printf("%-*d$\n",42, da);
	rex = printf("%-*d$\n",42, da);
	rex = printf("%d != %d\n",ret, rex);

	ret = ft_printf("%0*d$\n",42, c);
	rex = printf("%0*d$\n",42,c);
	rex = printf("%d != %d\n",ret, rex);
	ret = ft_printf("%0*d$\n",42, d);
	rex = printf("%0*d$\n",42,d);
	rex = printf("%d != %d\n",ret, rex);
	ret = ft_printf("%0*d$\n",42, da);
	rex = printf("%0*d$\n",42, da);
	rex = printf("%d != %d\n",ret, rex);
	
	ret = ft_printf("%10.5d$\n", 2);
	rex = printf("%10.5d$\n", 2);
	rex = printf("%d != %d\n",ret, rex);
	ret = ft_printf("%10.0d$\n", 0);
	rex = printf("%10.0d$\n", 0);
	rex = printf("%d != %d\n",ret, rex);
	
	ret = ft_printf("---------------- TEST 16 --------------\n\n");
	ret = ft_printf("%-8.u$\n", c);
	rex = printf("%-8.u$\n",c);
	rex = printf("%d != %d\n",ret, rex);
	ret = ft_printf("%-8.u$\n", d);
	rex = printf("%-8.u$\n",d);
	rex = printf("%d != %d\n",ret, rex);
	ret = ft_printf("%-8.u$\n", da);
	rex = printf("%-8.u$\n",da);
	rex = printf("%d != %d\n",ret, rex);

	ret = ft_printf("%12u$\n", c);
	rex = printf("%12u$\n",c);
	rex = printf("%d != %d\n",ret, rex);
	ret = ft_printf("%12u$\n", d);
	rex = printf("%12u$\n",d);
	rex = printf("%d != %d\n",ret, rex);
	ret = ft_printf("%12u$\n", da);
	rex = printf("%12u$\n",da);
	rex = printf("%d != %d\n",ret, rex);

	ret = ft_printf("%012.4u$\n", c);
	rex = printf("%12.4u$\n",c);
	rex = printf("%d != %d\n",ret, rex);
	ret = ft_printf("%012.4u$\n", d);
	rex = printf("%12.4u$\n",d);
	rex = printf("%d != %d\n",ret, rex);
	ret = ft_printf("%012.4u$\n", da);
	rex = printf("%12.4u$\n",da);
	rex = printf("%d != %d\n",ret, rex);


	ret = ft_printf("%0.4u$\n", c);
	rex = printf("%0.4u$\n",c);
	rex = printf("%d != %d\n",ret, rex);
	ret = ft_printf("%0.4u$\n", d);
	rex = printf("%0.4u$\n",d);
	rex = printf("%d != %d\n",ret, rex);
	ret = ft_printf("%0.4u$\n", da);
	rex = printf("%0.4u$\n",da);
	rex = printf("%d != %d\n",ret, rex);

	ret = ft_printf("%-40.*u$\n",42, c);
	rex = printf("%-40.*u$\n",42,c);
	rex = printf("%d != %d\n",ret, rex);
	ret = ft_printf("%-40.*u$\n",42, d);
	rex = printf("%-40.*u$\n",42,d);
	rex = printf("%d != %d\n",ret, rex);
	ret = ft_printf("%-40.*u$\n",42, da);
	rex = printf("%-40.*u$\n",42, da);
	rex = printf("%d != %d\n",ret, rex);

	ret = ft_printf("%-*u$\n",42, c);
	rex = printf("%-*u$\n",42,c);
	rex = printf("%d != %d\n",ret, rex);
	ret = ft_printf("%-*u$\n",42, d);
	rex = printf("%-*u$\n",42,d);
	rex = printf("%d != %d\n",ret, rex);
	ret = ft_printf("%-*u$\n",42, da);
	rex = printf("%-*u$\n",42, da);
	rex = printf("%d != %d\n",ret, rex);

	ret = ft_printf("%0*u$\n",42, c);
	rex = printf("%0*u$\n",42,c);
	rex = printf("%d != %d\n",ret, rex);
	ret = ft_printf("%0*u$\n",42, d);
	rex = printf("%0*u$\n",42,d);
	rex = printf("%d != %d\n",ret, rex);
	ret = ft_printf("%0*u$\n",42, da);
	rex = printf("%0*u$\n",42, da);
	rex = printf("%d != %d\n",ret, rex);
	
	ret = ft_printf("%10.5u$\n", 2);
	rex = printf("%10.5u$\n", 2);
	rex = printf("%d != %d\n",ret, rex);

	ret = ft_printf("---------------- TEST 17 --------------\n\n");
	ret = ft_printf("%-8.c$\n", c);
	rex = printf("%-8.c$\n",c);
	rex = printf("%d == %d\n", ret, rex);
	ret = ft_printf("%-8.c$\n", d);
	rex = printf("%-8.c$\n",d);
	rex = printf("%d == %d\n", ret, rex);
	ret = ft_printf("%-8.c$\n", da);
	rex = printf("%-8.c$\n",da);
	rex = printf("%d == %d\n", ret, rex);

	ret = ft_printf("%12c$\n", c);
	rex = printf("%12c$\n",c);
	rex = printf("%d == %d\n", ret, rex);
	ret = ft_printf("%12c$\n", d);
	rex = printf("%12c$\n",d);
	rex = printf("%d == %d\n", ret, rex);
	ret = ft_printf("%12c$\n", da);
	rex = printf("%12c$\n",da);
	rex = printf("%d == %d\n", ret, rex);


	ret = ft_printf("%-*c$\n",42, c);
	rex = printf("%-*c$\n",42,c);
	rex = printf("%d == %d\n", ret, rex);
	ret = ft_printf("%-*c$\n",42, d);
	rex = printf("%-*c$\n",42,d);
	rex = printf("%d == %d\n", ret, rex);
	ret = ft_printf("%-*c$\n",42, da);
	rex = printf("%-*c$\n",42, da);
	rex = printf("%d == %d\n", ret, rex);

	ret = ft_printf("%-0*c$\n",42, c);
	rex = printf("%-0*c$\n",42,c);
	rex = printf("%d == %d\n", ret, rex);
	ret = ft_printf("%-*c$\n",42, d);
	rex = printf("%-*c$\n",42,d);
	rex = printf("%d == %d\n", ret, rex);
	ret = ft_printf("%-*c$\n",42, da);
	rex = printf("%-*c$\n",42, da);
	rex = printf("%d == %d\n", ret, rex);

	ret = ft_printf("%%d 0000042 == |%d|\n", 0000042);
	rex = printf("%%d 0000042 == |%d|\n", 0000042);
	rex = printf("%d != %d\n",ret, rex);
	ret = ft_printf("%d\n", 0000042);
	rex = printf("%d\n", 0000042);
	rex = printf("%d != %d\n",ret, rex);

	ret = ft_printf("%d$\n", 2147483647 + 1);
	rex = printf("%d$\n", 2147483647 + 1);
	rex = printf("%d != %d\n",ret, rex);
	ret = ft_printf("%d$\n", -2147483648);
	rex = printf("%d$\n", -2147483648);
	rex = printf("%d != %d\n",ret, rex);
	ret = ft_printf("%0*i$\n", -7, -54);
	rex = printf("%0*i$\n", -7, -54);
	rex = printf("%d != %d\n",ret, rex);
	ret = ft_printf("%0*i$\n", 7, -54);
	rex = printf("%0*i$\n", 7, -54);
	rex = printf("%d != %d\n",ret, rex);

	ret = ft_printf("%% *.5i 42 == |%*.5i|\n", 4, 42);
	rex = printf("%% *.5i 42 == |%*.5i|\n", 4, 42);
	rex = printf("%d != %d\n",ret, rex);

	char alo[5] = "aloa";
	ret = ft_printf("$%d\n", rex = printf("1 [%s]$\n",alo));
	rex = printf("$%d\n", rex = printf("1 [%s]$\n",  alo));
	rex = printf("%d != %d\n",ret, rex);

	ret = ft_printf("-->|%-4.%|<--\n");
	rex = printf("-->|%-4.%|<--\n");
	rex = printf("%d != %d\n",ret, rex);
	ret = ft_printf("-->|%*.*%|<--\n", 2, -4);
	rex = printf("-->|%*.*%|<--\n", 2, -4);
	rex = printf("%d != %d\n",ret, rex);
	
	ret = ft_printf("---------------- TEST 11 --------------\n\n");
	ret = ft_printf("%-8.s$\n", g);
	rex = printf("%-8.s$\n",g);
	rex = printf("%d != %d\n",ret, rex);
	ret = ft_printf("%-8.s$\n", f);
	rex = printf("%-8.s$\n",f);
	rex = printf("%d != %d\n",ret, rex);
	ret = ft_printf("%-8.s$\n", e);
	rex = printf("%-8.s$\n",e);
	rex = printf("%d != %d\n",ret, rex);
	

	ret = ft_printf("%40.*s$\n",42, g);
	rex = printf("%40.*s$\n",42,g);
	rex = printf("%d != %d\n",ret, rex);
	ret = ft_printf("%-40.*s$\n",42, f);
	rex = printf("%-40.*s$\n",42,f);
	rex = printf("%d != %d\n",ret, rex);
	ret = ft_printf("%-40.*s$\n",42, e);
	rex = printf("%-40.*s$\n",42, e);
	rex = printf("%d != %d\n",ret, rex);

	ret = ft_printf("%-*s$\n",42, g);
	rex = printf("%-*s$\n",42,g);
	rex = printf("%d != %d\n",ret, rex);
	ret = ft_printf("%-*s$\n",42, f);
	rex = printf("%-*s$\n",42,f);
	rex = printf("%d != %d\n",ret, rex);
	ret = ft_printf("%-*s$\n",42, e);
	rex = printf("%-*s$\n",42, e);
	rex = printf("%d != %d\n",ret, rex);

	ret = ft_printf("%*s$\n",42, g);
	rex = printf("%*s$\n",42,g);
	rex = printf("%d != %d\n",ret, rex);
	ret = ft_printf("%*s$\n",42, f);
	rex = printf("%*s$\n",42,f);
	rex = printf("%d != %d\n",ret, rex);
	ret = ft_printf("%*s$\n",42, e);
	rex = printf("%*s$\n",42, e);
	rex = printf("%d != %d\n",ret, rex);

	char s[10] = "abcdefg";
	ret = ft_printf("-->|%*.0s|<--\n", -1, s);
	rex = printf("-->|%*.0s|<--\n", -1, s);
	rex = printf("%d != %d\n",ret, rex);
	ret = ft_printf("-->|%*.s|<--\n", -1, s);
	rex = printf("-->|%*.s|<--\n", -1, s);
	rex = printf("%d != %d\n",ret, rex);
	ret = ft_printf("-->|%*.*s|<--\n", -1, 0, s);
	rex = printf("-->|%*.*s|<--\n", -1, 0, s);
	rex = printf("%d != %d\n",ret, rex);
	ret = ft_printf("%.3s$\n", "holla", "bitch");
	rex = printf("%.3s$\n", "holla", "bitch");
	rex = printf("%d != %d\n",ret, rex);
	ret = ft_printf("%.7s$\n", "hello");
	rex = printf("%.7s$\n", "hello");
	rex = printf("%d != %d\n",ret, rex);

	char **p = &g;
	ret = ft_printf("---------------- TEST 12 --------------\n\n");
	ret = ft_printf("%--16.p$\n", p);
	rex = printf("%-16.p$\n",p);
	rex = printf("%d != %d\n",ret, rex);
	ret = ft_printf("%-8.p$\n", f);
	rex = printf("%-8.p$\n",f);
	rex = printf("%d != %d\n",ret, rex);
	ret = ft_printf("%-8.p$\n", e);
	rex = printf("%-8.p$\n",e);
	rex = printf("%d != %d\n",ret, rex);

	ret = ft_printf("%-*p$\n",42, g);
	rex = printf("%-*p$\n",42,g);
	rex = printf("%d != %d\n",ret, rex);
	ret = ft_printf("%-*p$\n",42, f);
	rex = printf("%-*p$\n",42,f);
	rex = printf("%d != %d\n",ret, rex);
	ret = ft_printf("%-*p$\n",42, e);
	rex = printf("%-*p$\n",42, e);
	rex = printf("%d != %d\n",ret, rex);

	ret = ft_printf("%*p$\n",42, g);
	rex = printf("%*p$\n",42,g);
	rex = printf("%d != %d\n",ret, rex);
	ret = ft_printf("%*p$\n",42, f);
	rex = printf("%*p$\n",42,f);
	rex = printf("%d != %d\n",ret, rex);
	ret = ft_printf("%*p$\n",42, e);
	rex = printf("%*p$\n",42, e);
	rex = printf("%d != %d\n",ret, rex);

	ret = ft_printf("%09p$\n", 1234);
	rex = printf("%09p$\n", 1234);
	rex = printf("%d != %d\n",ret, rex);
	
	return (0);
}
