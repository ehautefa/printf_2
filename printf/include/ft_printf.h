/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ehautefa <ehautefa@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/08 10:13:26 by ehautefa          #+#    #+#             */
/*   Updated: 2021/01/22 09:57:02 by ehautefa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H

# include <stdlib.h>
# include <stdio.h>
# include <stdarg.h>
# include "../libft/libft.h"

typedef struct	s_list_ptf
{
	char				*attribut;
	char				conversion;
	char				*element;
	struct s_list_ptf	*next;
}				t_list_ptf;

typedef struct	s_flag
{
	int		precision;
	int		p_mk;
	int		width;
	int		w_mk;
	int		justfication;
	int		zero;
}				t_flag;

int				ft_printf(const char *arg, ...);
char			*ft_mall_width(t_flag list, int size, char c);
char			*ft_mall_str_w(t_flag list, int size, char c);
char			*ft_s_crea(t_flag l, char *s);
t_list_ptf		*ft_lstnew_printf(char *attribut, char conversion);
void			ft_lstadd_back_printf(t_list_ptf **alst, t_list_ptf *new);
int				ft_lstsize_printf(t_list_ptf *lst);
t_list_ptf		*ft_lstlast_printf(t_list_ptf *lst);
void			ft_lstclear_printf(t_list_ptf *lst);
char			*ft_convert_base_printf(unsigned int nb, char *base_to);
t_flag			ft_flag(t_list_ptf *liste, va_list ap);
t_flag			ft_flag_init(void);
int				ft_catch_nb(char *str, int *ptr_i, int *marker, va_list arg);
int				ft_size_max(char *str, int *ptr_i);
void			ft_putnbr_without_protection(unsigned int nb, int size_max);
void			ft_print_address(unsigned long ad);
int				ft_s_conversion(t_list_ptf *liste, va_list ap);
int				ft_d_conversion(t_list_ptf *liste, va_list ap);
int				ft_c_conversion(t_list_ptf *liste, va_list ap);
char			*ft_u_crea(t_flag l, unsigned int nb);
int				ft_u_conversion(t_list_ptf *liste, va_list ap);
char			*ft_x_crea(t_flag l, unsigned int nb, char conv);
int				ft_x_conversion(t_list_ptf *liste, va_list ap);
int				ft_p_conversion(t_list_ptf *liste, va_list ap);
int				ft_no_conversion(t_list_ptf *liste, va_list ap);
t_flag			ft_control_flag(t_flag list);
char			*ft_mall_d_w(t_flag list, int size, char c);
char			*ft_d_crea_zero(t_flag l, int neg_mk, char *str);
char			*ft_d_crea(t_flag l, int nb);
int				ft_is_conversion(char c);
char			*find_attribut(char *arg, int *i, int *f_att);
t_list_ptf		*ft_create_struct(char *attribut);
t_list_ptf		*ft_create_list(const char *arg);
int				ft_conversion(t_list_ptf *liste, va_list ap);
int				ft_print_arg(t_list_ptf *lst_argument, char *arg, va_list ap);
void			ft_print_address(unsigned long ad);
int				ft_size_addresse(unsigned long ad);
int				ft_p_zero(void *entree, t_flag list_flag, int size);
int				ft_precision_zero(void *entree, t_flag list_flag, int size);

#endif
