/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ehautefa <ehautefa@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/08/10 17:41:42 by ehautefa          #+#    #+#             */
/*   Updated: 2021/01/22 09:55:42 by ehautefa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	check_base(char c, char *base, int j)
{
	int i;

	i = 0;
	while (base[i])
	{
		if (c == base[i])
		{
			if (j == 1)
				return (i);
			else
				return (1);
		}
		i++;
	}
	return (0);
}

static long	ft_lenmum(int i, char *str, char *base)
{
	long	to_print;
	int		base_len;

	to_print = 0;
	base_len = ft_strlen(base);
	while (check_base(str[i], base, 0))
	{
		to_print = to_print * base_len + check_base(str[i], base, 1);
		i++;
	}
	return (to_print);
}

int			ft_atoi_base(char *str, char *base)
{
	int count_moins;
	int	i;

	count_moins = 1;
	i = 0;
	while (str[i] == ' ' || str[i] == '\t' || str[i] == '\n' ||
			str[i] == '\v' || str[i] == '\f' || str[i] == '\r')
		i++;
	while (str[i] == '+' || str[i] == '-')
	{
		if (str[i] == '-')
			count_moins = -count_moins;
		i++;
	}
	return (count_moins * ft_lenmum(i, str, base));
}
