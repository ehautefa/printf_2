/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_convert_base.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ehautefa <ehautefa@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/08/14 12:04:35 by ehautefa          #+#    #+#             */
/*   Updated: 2021/01/22 09:56:26 by ehautefa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	ft_strlen_return(long nb, int base_to_len)
{
	int	size;

	size = 0;
	if (nb < 0)
	{
		nb = -nb;
		size++;
	}
	while (nb >= 1)
	{
		nb = nb / base_to_len;
		size++;
	}
	return (size);
}

static char	*ft_zero(int nb, char *base_to)
{
	char	*str;
	int		size;

	size = ft_strlen_return(nb, ft_strlen(base_to));
	if (!(str = (char *)malloc(size * sizeof(char) + 1)))
		return (NULL);
	if (nb == 0)
	{
		str[0] = base_to[0];
		str[1] = '\0';
	}
	return (str);
}

static char	*ft_control(char *nbr, char *base_from, char *base_to)
{
	if (!(ft_base_control(base_from)) || !(ft_base_control(base_to)))
		return (NULL);
	if (nbr == NULL)
		return ("0");
	return ("1");
}

char		*ft_convert_base(char *nbr, char *base_from, char *base_to)
{
	long		nb;
	int			i;
	char		*str;
	int			size;

	i = 1;
	if (ft_control(nbr, base_from, base_to) == 0)
		return (ft_control(nbr, base_from, base_to));
	nb = ft_atoi_base(nbr, base_from);
	size = ft_strlen_return(nb, ft_strlen(base_to));
	nb = nb < 0 ? -nb : nb;
	str = ft_zero(nb, base_to);
	if (nb == 0)
		return (str);
	str[0] = '-';
	while (nb >= 1)
	{
		str[size - i] = base_to[nb % ft_strlen(base_to)];
		nb = nb / ft_strlen(base_to);
		i++;
	}
	str[size] = '\0';
	return (str);
}
