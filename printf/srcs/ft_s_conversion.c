/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_s_conversion.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ehautefa <ehautefa@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/18 08:48:30 by ehautefa          #+#    #+#             */
/*   Updated: 2021/01/22 09:49:10 by ehautefa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/ft_printf.h"

char	*ft_mall_str_w(t_flag list, int size, char c)
{
	int		i;
	char	*str;

	i = -1;
	if (list.p_mk == 1 && list.w_mk == 1)
	{
		if (list.precision < size)
			size = list.width - list.precision;
		else
			size = list.width - size;
	}
	else if (list.p_mk == 0 && list.w_mk == 1)
		size = list.width - size;
	else
		size = 0;
	if (size < 0)
		size = 0;
	str = malloc((size + 1) * sizeof(char));
	if (str == NULL)
		return (NULL);
	while (++i < size)
		str[i] = c;
	str[i] = '\0';
	return (str);
}

char	*ft_s_crea(t_flag l, char *s)
{
	if (s == NULL)
		s = "(null)";
	if (l.p_mk == 1 && s)
		s = ft_substr(s, 0, l.precision);
	else
		s = ft_substr(s, 0, ft_strlen(s));
	return (s);
}

int		ft_s_conversion(t_list_ptf *liste, va_list ap)
{
	t_flag	l;
	char	*s;
	char	*str;
	int		size;
	char	c;

	c = ' ';
	l = ft_flag(liste, ap);
	s = va_arg(ap, char *);
	if (!(s = ft_s_crea(l, s)))
		return (-1);
	if (l.zero == 1)
		c = '0';
	str = ft_mall_str_w(l, ft_strlen(s), c);
	if (l.justfication == 0)
		str = ft_strjoin(str, s);
	else
		str = ft_strjoin(s, str);
	if (str == NULL)
		return (-1);
	size = ft_strlen(str);
	write(1, str, size);
	free(str);
	str = NULL;
	return (size);
}

int		ft_no_conversion(t_list_ptf *liste, va_list ap)
{
	char	c;
	t_flag	l;
	char	*str;
	int		i;

	i = 0;
	c = ' ';
	l = ft_flag(liste, ap);
	if (l.zero == 1)
		c = '0';
	if (!(str = ft_mall_width(l, 1, c)))
		return (-1);
	while (str[i])
		i++;
	if (l.justfication == 0)
		str[i - 1] = '%';
	else
		str[0] = '%';
	write(1, str, i);
	free(str);
	str = NULL;
	return (i);
}
