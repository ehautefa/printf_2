/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_u_conversion.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ehautefa <ehautefa@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/20 13:31:01 by ehautefa          #+#    #+#             */
/*   Updated: 2021/01/22 07:13:05 by ehautefa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/ft_printf.h"

char			*ft_u_crea(t_flag l, unsigned int nb)
{
	char			*str;
	char			*zero;

	if (l.precision == 0 && nb == 0 && l.p_mk == 1)
		str = ft_strnew(0);
	else
		str = ft_itoa_unsigned(nb);
	if (str == NULL)
		return (NULL);
	if (!(zero = ft_d_crea_zero(l, 0, str)))
		return (NULL);
	str = ft_strjoin(zero, str);
	return (str);
}

int				ft_u_conversion(t_list_ptf *liste, va_list ap)
{
	t_flag	l;
	char	*s;
	char	*str;
	int		size;
	int		unsign;

	l = ft_flag(liste, ap);
	unsign = (unsigned int)va_arg(ap, unsigned int);
	s = ft_u_crea(l, unsign);
	if (s == NULL)
		return (-1);
	str = ft_mall_d_w(l, ft_strlen(s), ' ');
	if (l.justfication == 0)
		str = ft_strjoin(str, s);
	else
		str = ft_strjoin(s, str);
	size = ft_strlen(str);
	write(1, str, size);
	free(str);
	str = NULL;
	return (size);
}
