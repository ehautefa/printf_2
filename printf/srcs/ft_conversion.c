/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_conversion.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ehautefa <ehautefa@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/20 13:27:25 by ehautefa          #+#    #+#             */
/*   Updated: 2021/01/22 07:11:57 by ehautefa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/ft_printf.h"

int			ft_conversion(t_list_ptf *liste, va_list ap)
{
	if (liste->conversion == 'c')
		return (ft_c_conversion(liste, ap));
	else if (liste->conversion == 's')
		return (ft_s_conversion(liste, ap));
	else if (liste->conversion == 'p')
		return (ft_p_conversion(liste, ap));
	else if (liste->conversion == 'd' || liste->conversion == 'i')
		return (ft_d_conversion(liste, ap));
	else if (liste->conversion == 'u')
		return (ft_u_conversion(liste, ap));
	else if (liste->conversion == 'X' || liste->conversion == 'x')
		return (ft_x_conversion(liste, ap));
	else if (liste->conversion == '%')
		return (ft_no_conversion(liste, ap));
	return (-1);
}

int			ft_is_conversion(char c)
{
	if (c == 'c')
		return (0);
	else if (c == 's')
		return (0);
	else if (c == 'p')
		return (0);
	else if (c == 'd' || c == 'i')
		return (0);
	else if (c == 'u')
		return (0);
	else if (c == 'X')
		return (0);
	else if (c == 'x')
		return (0);
	else if (c == '%')
		return (0);
	return (1);
}
