/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_liste_chaine.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ehautefa <ehautefa@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/08 10:11:13 by ehautefa          #+#    #+#             */
/*   Updated: 2021/01/22 07:12:27 by ehautefa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/ft_printf.h"

char		*find_attribut(char *arg, int *i, int *f_att)
{
	int		start;
	char	*attribut;

	while (arg && arg[*i])
	{
		if (arg[*i] == '%')
		{
			start = *i;
			*i += 1;
			while (ft_is_conversion(arg[*i]))
				*i += 1;
			if (!(attribut = ft_substr(arg, start + 1, *i - start)))
			{
				*f_att = -1;
				return (NULL);
			}
			*i += 1;
			*f_att = 1;
			return (attribut);
		}
		*i += 1;
	}
	*f_att = 0;
	return (0);
}

t_list_ptf	*ft_create_struct(char *attribut)
{
	t_list_ptf	*lst_data;
	char		conversion;
	int			j;

	j = 0;
	while (attribut && attribut[j])
		j++;
	conversion = attribut[j - 1];
	attribut[j - 1] = '\0';
	if (!(lst_data = ft_lstnew_printf(attribut, conversion)))
		return (NULL);
	return (lst_data);
}

t_list_ptf	*ft_create_list(const char *arg)
{
	char		*attribut;
	int			i;
	t_list_ptf	*lst_data;
	t_list_ptf	*tmp;
	int			flag_find_attribut;

	i = 0;
	attribut = NULL;
	attribut = find_attribut((char *)arg, &i, &flag_find_attribut);
	if (flag_find_attribut == 0)
		return (NULL);
	lst_data = ft_create_struct(attribut);
	tmp = lst_data;
	attribut = find_attribut((char *)arg, &i, &flag_find_attribut);
	while (flag_find_attribut == 1)
	{
		lst_data->next = ft_create_struct(attribut);
		if (lst_data == NULL)
			return (NULL);
		lst_data = lst_data->next;
		attribut = find_attribut((char *)arg, &i, &flag_find_attribut);
	}
	if (flag_find_attribut == -1)
		return (NULL);
	return (tmp);
}

int			ft_print_arg(t_list_ptf *lst_argument, char *arg, va_list ap)
{
	int	i;
	int	retur;
	int	ctrl;

	i = 0;
	retur = 0;
	while (arg && arg[i])
	{
		while (arg[i] && arg[i] != '%')
		{
			ft_putchar((unsigned char)arg[i]);
			i++;
			retur++;
		}
		if (arg[i] == '\0')
			return (retur);
		i = i + ft_strlen(lst_argument->attribut) + 2;
		ctrl = ft_conversion(lst_argument, ap);
		if (ctrl == -1)
			return (-1);
		retur += ctrl;
		lst_argument = lst_argument->next;
	}
	return (retur);
}

int			ft_printf(const char *arg, ...)
{
	va_list		ap;
	t_list_ptf	*lst_argument;
	int			retur;

	va_start(ap, arg);
	lst_argument = ft_create_list(arg);
	retur = ft_print_arg(lst_argument, (char *)arg, ap);
	if (retur == -1)
		ft_putstr("BAD ARGUMENT");
	ft_lstclear_printf(lst_argument);
	va_end(ap);
	return (retur);
}
