/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_c_conversion.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ehautefa <ehautefa@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/10 10:17:36 by ehautefa          #+#    #+#             */
/*   Updated: 2021/01/22 07:11:46 by ehautefa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/ft_printf.h"

char	*ft_mall_width(t_flag list, int size, char c)
{
	char	*str;
	int		i;

	i = 0;
	if (list.w_mk == 1 && list.width > size)
		size = list.width;
	str = malloc((size + 1) * sizeof(char));
	if (!str)
		return (NULL);
	while (i < size)
	{
		str[i] = c;
		i++;
	}
	str[i] = '\0';
	return (str);
}

int		ft_c_conversion(t_list_ptf *liste, va_list ap)
{
	char	c;
	t_flag	l;
	char	*str;
	int		i;

	i = 0;
	l = ft_flag(liste, ap);
	c = va_arg(ap, int);
	if (!(str = ft_mall_width(l, 1, ' ')))
		return (-1);
	while (str[i])
		i++;
	if (l.justfication == 0)
		str[i - 1] = c;
	else
		str[0] = c;
	write(1, str, i);
	free(str);
	str = NULL;
	return (i);
}
