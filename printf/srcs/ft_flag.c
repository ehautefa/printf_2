/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_flag.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ehautefa <ehautefa@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/11 11:36:18 by ehautefa          #+#    #+#             */
/*   Updated: 2021/01/22 07:12:21 by ehautefa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/ft_printf.h"

t_flag	ft_flag_init(void)
{
	t_flag liste;

	liste.justfication = 0;
	liste.p_mk = 0;
	liste.w_mk = 0;
	liste.width = 0;
	liste.precision = 0;
	liste.zero = 0;
	return (liste);
}

int		ft_catch_nb(char *str, int *ptr_i, int *marker, va_list arg)
{
	int nb;
	int j;

	j = 0;
	nb = 0;
	if (str[j] == '.')
	{
		j++;
		*ptr_i += 1;
	}
	if (str[j] == '*')
		nb = va_arg(arg, int);
	else if (str[j] >= '0' && str[j] <= '9')
	{
		nb = ft_atoi(&str[j]);
		while (str[j] >= '0' && str[j] <= '9')
		{
			j++;
			*ptr_i += 1;
		}
		*ptr_i -= 1;
	}
	*marker = 1;
	return (nb);
}

t_flag	ft_control_flag(t_flag list)
{
	if (list.precision < 0)
	{
		list.precision = 0;
		list.p_mk = 0;
	}
	if (list.width < 0)
	{
		list.width = -list.width;
		list.justfication = 1;
	}
	if (list.justfication == 1 && list.zero == 1)
		list.zero = 0;
	return (list);
}

t_flag	ft_flag(t_list_ptf *liste, va_list ap)
{
	char	*flag;
	int		i;
	t_flag	l_flag;

	i = 0;
	flag = liste->attribut;
	l_flag = ft_flag_init();
	while (flag && flag[i] != '\0')
	{
		if (flag[i] == '-')
			l_flag.justfication = 1;
		else if (flag[i] == '*')
		{
			l_flag.width = va_arg(ap, int);
			l_flag.w_mk = 1;
		}
		else if (flag[i] == '.')
			l_flag.precision = ft_catch_nb(&flag[i], &i, &l_flag.p_mk, ap);
		else if (flag[i] == '0')
			l_flag.zero = 1;
		else if (flag[i] > '0' && flag[i] <= '9')
			l_flag.width = ft_catch_nb(&flag[i], &i, &l_flag.w_mk, ap);
		i++;
	}
	return (ft_control_flag(l_flag));
}
