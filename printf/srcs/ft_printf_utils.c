/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_utils.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ehautefa <ehautefa@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/08 10:12:27 by ehautefa          #+#    #+#             */
/*   Updated: 2021/01/22 07:12:45 by ehautefa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/ft_printf.h"

t_list_ptf	*ft_lstnew_printf(char *attribut, char conversion)
{
	t_list_ptf	*tab;

	if (!(tab = malloc(sizeof(t_list_ptf))))
		return (NULL);
	tab->attribut = attribut;
	tab->conversion = conversion;
	tab->next = NULL;
	return (tab);
}

void		ft_lstadd_back_printf(t_list_ptf **alst, t_list_ptf *new)
{
	t_list_ptf	*last;

	if (*alst == NULL)
	{
		*alst = new;
		return ;
	}
	last = ft_lstlast_printf(*alst);
	last->next = new;
}

int			ft_lstsize_printf(t_list_ptf *lst)
{
	int	size;

	size = 0;
	while (lst)
	{
		size++;
		lst = lst->next;
	}
	return (size);
}

t_list_ptf	*ft_lstlast_printf(t_list_ptf *lst)
{
	while (lst)
	{
		if (lst->next == NULL)
			return (lst);
		else
			lst = lst->next;
	}
	return (lst);
}

void		ft_lstclear_printf(t_list_ptf *lst)
{
	t_list_ptf	*tmp;

	while (lst)
	{
		tmp = lst->next;
		free(lst->attribut);
		free(lst);
		lst = tmp;
	}
}
