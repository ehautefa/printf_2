/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_x_conversion.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ehautefa <ehautefa@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/20 12:10:12 by ehautefa          #+#    #+#             */
/*   Updated: 2021/01/22 07:12:37 by ehautefa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/ft_printf.h"

char	*ft_x_crea(t_flag l, unsigned int nb, char conv)
{
	char			*str;
	char			*zero;

	if (l.precision == 0 && nb == 0 && l.p_mk == 1)
		str = ft_strnew(0);
	else if (conv == 'x')
		str = ft_convert_base_printf(nb, "0123456789abcdef");
	else
		str = ft_convert_base_printf(nb, "0123456789ABCDEF");
	if (str == NULL)
		return (NULL);
	if (!(zero = ft_d_crea_zero(l, 0, str)))
		return (NULL);
	str = ft_strjoin(zero, str);
	return (str);
}

int		ft_x_conversion(t_list_ptf *liste, va_list ap)
{
	t_flag			l;
	char			*s;
	char			*str;
	int				size;
	unsigned int	d;

	l = ft_flag(liste, ap);
	d = va_arg(ap, unsigned int);
	s = ft_x_crea(l, d, liste->conversion);
	if (s == NULL)
		return (-1);
	str = ft_mall_d_w(l, ft_strlen(s), ' ');
	if (l.justfication == 0)
		str = ft_strjoin(str, s);
	else
		str = ft_strjoin(s, str);
	size = ft_strlen(str);
	write(1, str, size);
	free(str);
	str = NULL;
	return (size);
}
