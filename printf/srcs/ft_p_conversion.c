/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_p_conversion.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ehautefa <ehautefa@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/19 11:57:49 by ehautefa          #+#    #+#             */
/*   Updated: 2021/01/22 07:12:51 by ehautefa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/ft_printf.h"

void	ft_print_address(unsigned long ad)
{
	char	*base;

	base = "0123456789abcdef";
	if ((void *)ad == NULL)
		return ;
	if (ad < 16)
		ft_putchar(base[ad]);
	else
	{
		ft_print_address(ad / 16);
		ft_putchar(base[ad % 16]);
	}
}

int		ft_size_addresse(unsigned long ad)
{
	int	size;

	size = 0;
	while (ad >= 1)
	{
		ad /= 16;
		size++;
	}
	return (size);
}

int		ft_p_zero(void *entree, t_flag list_flag, int size)
{
	char	*str;
	int		len_str;
	char	*prefixe;
	int		pre_mk;

	prefixe = "0x0";
	pre_mk = 0;
	if (list_flag.p_mk == 0 && entree == NULL)
		pre_mk++;
	if (!(str = ft_mall_str_w(list_flag, size + pre_mk, '0')))
		return (-1);
	write(1, prefixe, 2 + pre_mk);
	write(1, str, ft_strlen(str));
	ft_print_address((unsigned long)entree);
	len_str = ft_strlen(str);
	free(str);
	return (len_str + pre_mk);
}

int		ft_precision_zero(void *entree, t_flag list_flag, int size)
{
	char	*str;
	int		len_str;
	char	*prefixe;
	int		pre_mk;

	prefixe = "0x0";
	pre_mk = 0;
	if (list_flag.p_mk == 0 && entree == NULL)
		pre_mk++;
	list_flag.p_mk = 0;
	if (!(str = ft_mall_str_w(list_flag, size + pre_mk, ' ')))
		return (-1);
	if (list_flag.justfication == 0)
		write(1, str, ft_strlen(str));
	write(1, prefixe, 2 + pre_mk);
	ft_print_address((unsigned long)entree);
	if (list_flag.justfication == 1)
		write(1, str, ft_strlen(str));
	len_str = ft_strlen(str);
	free(str);
	return (len_str + pre_mk);
}

int		ft_p_conversion(t_list_ptf *liste, va_list ap)
{
	void	*entree;
	t_flag	list_flag;
	int		ret;
	int		size;

	list_flag = ft_flag(liste, ap);
	entree = va_arg(ap, void *);
	if (list_flag.precision == 0 && entree == NULL)
		size = 2;
	else if (entree == NULL)
		size = 3;
	else
		size = ft_size_addresse((unsigned long)entree) + 2;
	if (list_flag.zero == 0)
		ret = ft_precision_zero(entree, list_flag, size);
	else
		ret = ft_p_zero(entree, list_flag, size);
	if (ret == -1)
		return (-1);
	return (ret + size);
}
