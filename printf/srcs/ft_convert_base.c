/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_convert_base.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ehautefa <ehautefa@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/08/14 12:04:35 by ehautefa          #+#    #+#             */
/*   Updated: 2021/01/22 07:12:06 by ehautefa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/ft_printf.h"

static int		ft_strlen_return(long nb, int base_to_len)
{
	int	size;

	size = 0;
	while (nb >= 1)
	{
		nb = nb / base_to_len;
		size++;
	}
	return (size);
}

static char		*ft_zero(int nb, char *base_to)
{
	char	*str;
	int		size;

	size = ft_strlen_return(nb, ft_strlen(base_to));
	if (!(str = (char *)malloc(size * sizeof(char) + 1)))
		return (NULL);
	if (nb == 0)
	{
		str[0] = base_to[0];
		str[1] = '\0';
	}
	return (str);
}

char			*ft_convert_base_printf(unsigned int nb, char *base_to)
{
	int			i;
	char		*str;
	int			size;

	i = 1;
	size = ft_strlen_return(nb, ft_strlen(base_to));
	str = ft_zero(nb, base_to);
	if (nb == 0)
		return (str);
	while (nb >= 1)
	{
		str[size - i] = base_to[nb % ft_strlen(base_to)];
		nb = nb / ft_strlen(base_to);
		i++;
	}
	str[size] = '\0';
	return (str);
}
