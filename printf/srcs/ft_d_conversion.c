/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_d_conversion.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ehautefa <ehautefa@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/20 09:15:08 by ehautefa          #+#    #+#             */
/*   Updated: 2021/01/22 09:43:52 by ehautefa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/ft_printf.h"

char			*ft_mall_d_w(t_flag list, int size, char c)
{
	int		i;
	char	*str;

	i = -1;
	if (list.p_mk == 1 && list.w_mk == 1)
	{
		if (list.precision > size)
			size = list.width - list.precision;
		else
			size = list.width - size;
	}
	else if (list.p_mk == 0 && list.w_mk == 1)
		size = list.width - size;
	else
		size = 0;
	if (size < 0)
		size = 0;
	str = malloc((size + 1) * sizeof(char));
	if (str == NULL)
		return (NULL);
	while (++i < size)
		str[i] = c;
	str[i] = '\0';
	return (str);
}

char			*ft_d_crea_zero(t_flag l, int neg_mk, char *str)
{
	char	*zero;
	int		i;
	int		size;

	i = -1;
	size = 0;
	if (l.p_mk == 0 && l.zero == 1)
	{
		l.precision = l.width;
		if (neg_mk == 1)
			size--;
	}
	size += neg_mk + l.precision - ft_strlen(str);
	if (size > 0)
		zero = ft_strnew(size);
	else
		zero = ft_strnew(0);
	if (zero == NULL)
		return (NULL);
	while (++i < size)
		zero[i] = '0';
	if (neg_mk == 1)
		zero[0] = '-';
	return (zero);
}

char			*ft_d_crea(t_flag l, int nb)
{
	char			*str;
	unsigned int	n;
	char			*zero;
	int				neg_mk;

	neg_mk = 0;
	if (nb < 0)
	{
		n = (unsigned int)nb * -1;
		neg_mk = 1;
	}
	else
		n = nb;
	if (l.precision == 0 && n == 0 && l.p_mk == 1)
		str = ft_strnew(0);
	else
		str = ft_itoa_unsigned(n);
	if (str == NULL)
		return (NULL);
	if (!(zero = ft_d_crea_zero(l, neg_mk, str)))
		return (NULL);
	str = ft_strjoin(zero, str);
	return (str);
}

int				ft_d_conversion(t_list_ptf *liste, va_list ap)
{
	t_flag	l;
	char	*s;
	char	*str;
	int		size;

	l = ft_flag(liste, ap);
	s = ft_d_crea(l, va_arg(ap, int));
	if (s == NULL)
		return (-1);
	if (!(str = ft_mall_d_w(l, ft_strlen(s), ' ')))
		return (-1);
	if (l.justfication == 0)
		str = ft_strjoin(str, s);
	else
		str = ft_strjoin(s, str);
	if (str == NULL)
		return (-1);
	size = ft_strlen(str);
	write(1, str, size);
	free(str);
	str = NULL;
	return (size);
}
